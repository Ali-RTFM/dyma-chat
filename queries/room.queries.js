const { Room } = require("../database/models");

exports.getRooms = () => {
  return Room.find({}).sort({ index: 1 }).exec();
};

exports.createRoom = async (room) => {
  const nbOfRooms = await Room.countDocuments().exec();
  const newRoom = new Room({
    index: nbOfRooms,
    title: room.title,
  });
  return newRoom.save();
};

exports.updateRoom = (room) => {
  return Room.findByIdAndUpdate(
    { _id: room._id },
    {
      index: room.index,
      title: room.title,
    }
  ).exec();
};

exports.deleteRoom = async (room) => {
  await Room.updateMany(
    { index: { $gt: room.index } },
    { $inc: { index: -1 } }
  );
  return Room.findByIdAndDelete({ _id: room._id }).exec();
};
